FROM cern/alma9-base

LABEL image.author="beatrice.jelmini@cern.ch"
LABEL version="1.0"
LABEL description="This is a test image. :)"


# update and upgrade everything as root
USER root
RUN dnf -qq -y update && \
   dnf -qq -y upgrade && \
   dnf -y clean all && \
   dnf -y autoremove && \
   rm -rf /var/lib/apt/lists/*

# Create user "jelmini"
RUN useradd -m jelmini && \
   cp /root/.bashrc /home/jelmini/ && \
   mkdir /home/jelmini/data && \
   chown -R --from=root jelmini /home/jelmini
ENV HOME /home/jelmini
# set current workind directory
WORKDIR ${HOME}/data
USER jelmini

COPY text.txt ${HOME}/

RUN \ 
    echo -e "#!/bin/bash\necho Hello World!" >> ${HOME}/helloworld.sh && \
	echo -e "echo user: $( whoami )" >> ${HOME}/helloworld.sh && \
	echo -e "echo current dir: $( pwd )" >> ${HOME}/helloworld.sh && \
	echo -e "cat /home/jelmini/text.txt" >> ${HOME}/helloworld.sh && \
    chmod +x ${HOME}/helloworld.sh

ENTRYPOINT ["/bin/bash", "/home/jelmini/helloworld.sh"]

